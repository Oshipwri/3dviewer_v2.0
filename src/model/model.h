#ifndef MODEL_H
#define MODEL_H
#include <QFile>

#include <QString>
#include <QVector2D>
#include <QVector3D>
#include <QVector>
#include <QDebug>
#include <QTextStream>


namespace s21 {

class Model {
 public:
    Model() : num_vert(0), num_edgs(0) {}
    ~Model() {}

    void get_data(QString path, QList<QVector3D> *coords,
                  QList<unsigned> *vertices, float *max);

    size_t get_num_vertices() const { return num_vert; }
    size_t get_num_edges() const { return num_edgs; }

 private:
    size_t num_vert, num_edgs;
};

}  // namespace s21

#endif  // MODEL_H
