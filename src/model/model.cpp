#include "model.h"

namespace s21 {

void Model::get_data(QString path, QList<QVector3D> *coords,
                     QList<unsigned> *vertices, float *max) {
    QFile objFile(path);

    if (!objFile.open(QIODevice::ReadOnly)) {
        qDebug() << "File not open";
        return;
    }
    QTextStream input(&objFile);

    while (!input.atEnd()) {
        QString str = input.readLine();
        QStringList list = str.split(" ");
        if (list[0] == "v") {
            QVector3D vertex(list[1].toFloat(), list[2].toFloat(),
                             list[3].toFloat());
            for (int i = 0; i < 3; ++i) {
                if (*max < vertex[i]) *max = vertex[i];
            }
            coords->push_back(vertex);
            continue;
        } else if (list[0] == "f") {
            QString sep = (list[1].contains("//")) ? "//" : (list[1].contains("/")) ? "/" : " ";

            for (int i = 1; i < list.size() - 1; ++i) {
                QStringList v_index = list[i].split(sep);
                vertices->push_back(v_index[0].toUInt() - 1);
            }

            if (list[list.size() - 1] != "") {
                QStringList v_index = list[list.size() - 1].split(sep);
                vertices->push_back(v_index[0].toUInt() - 1);
            }
        }
    }

    num_vert = coords->size();
    num_edgs = vertices->size();
}

}  // namespace s21
