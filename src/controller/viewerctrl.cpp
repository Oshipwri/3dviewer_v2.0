#include "viewerctrl.h"

namespace s21 {

void ViewerController::get_data(QString path, QList<QVector3D> *coords,
                                QList<unsigned> *vertices, float *max) {
    model_->get_data(path, coords, vertices, max);
}

size_t ViewerController::get_num_vertices() {
    return model_->get_num_vertices();
}

size_t ViewerController::get_num_edges() { return model_->get_num_edges(); }

}  // namespace s21
