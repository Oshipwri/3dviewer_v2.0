#pragma once
#include "../model/model.h"

namespace s21 {

class ViewerController {
 public:
    ViewerController(Model *model) : model_(model) {}
    ~ViewerController() {}

    void get_data(QString path, QList<QVector3D> *coords,
                  QList<unsigned> *vertices, float *max);
    size_t get_num_vertices();
    size_t get_num_edges();

 private:
    Model *model_;
};

}  // namespace s21
