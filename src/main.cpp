#include <QApplication>
#include <QFile>
#include <QFont>
#include <QFontDatabase>

#include "./controller/viewerctrl.h"
#include "./view/viewerwindow.h"

void applySettings(QApplication* p_app);

int main(int argc, char* argv[]) {
    QApplication viewerApp(argc, argv);
    QApplication::setOrganizationName("Fscourge");
    QApplication::setApplicationName("3D_viewer_v2.0");

    applySettings(&viewerApp);

    s21::Model model;
    s21::ViewerController C(&model);
    s21::ViewerWindow W(&C);

    W.showMaximized();
    return viewerApp.exec();
}

void applySettings(QApplication* p_app) {
    QFile styleSheet(":/Style/ViewerStyle.qss");
    QFile monaco_font(":/Style/Monaco.ttf");
    if (styleSheet.open(QIODevice::ReadOnly | QIODevice::Text) &&
        monaco_font.open(QIODevice::ReadOnly)) {
        int font_id =
            QFontDatabase::addApplicationFontFromData(monaco_font.readAll());
        QString family = QFontDatabase::applicationFontFamilies(font_id).at(0);
        QFont monaco(family);
        p_app->setStyleSheet(styleSheet.readAll());
        p_app->setFont(monaco);
        styleSheet.close();
        monaco_font.close();
    }
}
