#!/bin/bash

UNAME_S=$(uname -s)
APT=$(which apt)
    if [[ ! -z "$APT"  && "$UNAME_S" == Linux ]];then 
        CONVERT=$(which convert)
        if [ -z "$CONVERT" ];then 
            echo "ImageMagic is not installed. GIF conversion is not available
            Do you want to install it? [Y/n]"
            read ANSWER
            if [[ "$ANSWER" == "Y" || "$ANSWER" == "y" ]];then
                sudo apt install graphicsmagick-imagemagick-compat
            fi
        else
            echo "ImageMagic is already installed"
    fi
fi