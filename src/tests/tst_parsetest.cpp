#include <QTest>

#include "../model/model.h"

namespace s21 {
class ParseTest : public QObject {
    Q_OBJECT

 private slots:
    void CubeNumsAndMargins();
    void SphereNumsAndMargins();
};

void ParseTest::CubeNumsAndMargins() {
    Model model;
    QString path(":/Models/cube.obj");
    QVector<QVector3D> coords;
    QVector<unsigned> vertices;
    float max = 0;

    model.get_data(path, &coords, &vertices, &max);
    QCOMPARE(model.get_num_vertices(), 8);
    QCOMPARE(coords[0], QVector3D(1.0, -1.0, -1.0));
    QCOMPARE(coords[coords.size() - 1], QVector3D(-1.0, 1.0, -1.0));
    QCOMPARE(model.get_num_edges(), 36);
}

void ParseTest::SphereNumsAndMargins() {
    Model model;
    QString path(":/Models/sphere.obj");
    QVector<QVector3D> coords;
    QVector<unsigned> vertices;
    float max = 0;
    model.get_data(path, &coords, &vertices, &max);
    QCOMPARE(model.get_num_vertices(), 162);
    QCOMPARE(coords[0], QVector3D(0.0, 0.5, -2.54558420181));
    QCOMPARE(coords[coords.size() - 1],
             QVector3D(0.921001434326, -0.996241569519, -1.84201455116));
    QCOMPARE(model.get_num_edges(), 960);
}

}  // namespace s21

QTEST_APPLESS_MAIN(s21::ParseTest)

#include "tst_parsetest.moc"
