#include "settings.h"

namespace s21 {
Settings::Settings(QOpenGLWidget *openGLWidget) : openGLWidget_(openGLWidget) {
    load_settings();
}

QColor Settings::get_edge_color() { return edge_color_; }

QColor Settings::get_vertex_color() { return vertex_color_; }

QColor Settings::get_background_color() { return background_color_; }

QColor Settings::get_grid_color() { return grid_color_; }

Settings::Projection Settings::get_projection() { return projection_; }

Settings::TypeOfEdges Settings::get_edges_type() { return edges_type_; }

Settings::TypeOfVertices Settings::get_vertices_type() {
    return vertices_type_;
}

int Settings::get_edge_thickness() { return edge_thickness_; }

int Settings::get_vertex_size() { return vertex_size_; }

void Settings::set_projection(Projection projection) {
    projection_ = projection;
}

void Settings::set_edges_type(TypeOfEdges edges_type) {
    edges_type_ = edges_type;
    openGLWidget_->update();
}

void Settings::set_edge_thickness(int edge_thickness) {
    edge_thickness_ = edge_thickness;
    openGLWidget_->update();
}

void Settings::set_edge_color(QColor edge_color) {
    edge_color_ = edge_color;
    openGLWidget_->update();
}

void Settings::set_vertices_type(TypeOfVertices vertices_type) {
    vertices_type_ = vertices_type;
    openGLWidget_->update();
}

void Settings::set_vertex_size(int vertex_size) {
    vertex_size_ = vertex_size;
    openGLWidget_->update();
}

void Settings::set_vertex_color(QColor vertex_color) {
    vertex_color_ = vertex_color;
    openGLWidget_->update();
}

void Settings::set_background_color(QColor background_color) {
    background_color_ = background_color;
    openGLWidget_->update();
}

void Settings::set_grid_color(QColor grid_color) {
    grid_color_ = grid_color;
    openGLWidget_->update();
}

void Settings::save_settings() {
    current_settings.setValue("Projection", projection_);
    current_settings.setValue("TypeOfEdges", edges_type_);
    current_settings.setValue("EdgeThickness", edge_thickness_);
    current_settings.setValue("EdgeColor", edge_color_);

    current_settings.setValue("TypeOfVertices", vertices_type_);
    current_settings.setValue("VertexSize", vertex_size_);
    current_settings.setValue("VerticesColor", vertex_color_);

    current_settings.setValue("BackgroundColor", background_color_);
    current_settings.setValue("GridColor", grid_color_);
}

void Settings::load_settings() {
    Projection current_projection = (Projection) current_settings
            .value("Projection", Projection::Central)
            .toInt();

    TypeOfEdges current_edge_type = (TypeOfEdges) current_settings
            .value("TypeOfEdges", TypeOfEdges::Solid)
            .toInt();
    set_edges_type(current_edge_type);
    set_projection(current_projection);

    int current_edge_thickness = current_settings.value("EdgeThickness", 5).toInt();
    set_edge_thickness(current_edge_thickness);

    QColor current_edge_color = current_settings.value("EdgeColor", QColor(Qt::white))
            .value<QColor>();
    set_edge_color(current_edge_color);

    TypeOfVertices current_vertex_type = (TypeOfVertices) current_settings
            .value("TypeOfVertices", TypeOfVertices::None)
            .toInt();

    set_vertices_type(current_vertex_type);

    int current_vertex_size = current_settings.value("VertexSize", 5).toInt();
    set_vertex_size(current_vertex_size);

    QColor current_vertex_color = current_settings.value("VerticesColor", QColor(Qt::white))
            .value<QColor>();
    set_vertex_color(current_vertex_color);

    QColor current_background_color = current_settings.value("BackgroundColor", QColor(Qt::black))
            .value<QColor>();
    set_background_color(current_background_color);

    QColor current_grid_color = current_settings.value("GridColor", QColor(Qt::blue))
            .value<QColor>();
    set_grid_color(current_grid_color);
}

}  // namespace s21
