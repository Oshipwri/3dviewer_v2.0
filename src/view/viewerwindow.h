#ifndef VIEWERWINDOW_H
#define VIEWERWINDOW_H

#include <QColorDialog>
#include <QFileDialog>
#include <QMainWindow>
#include <QTimer>

#include "../controller/viewerctrl.h"
#include "customizemodel.h"
#include "settings.h"
#include "ui_viewerwindow.h"
#include "vieweropengl.h"

namespace Ui {
class ViewerWindow;
}

namespace s21 {

enum class SideBar : bool { VISIBLE, COLLAPSED };

class ViewerWindow : public QMainWindow {
    Q_OBJECT

 public:
    ViewerWindow(ViewerController *, QWidget *parent = nullptr);
    ~ViewerWindow();
    ViewerOpenGL *get_p_openGL();

 protected:
    void set_spinboxes_to_default();

 private:
    Ui::ViewerWindow *ui;
    ViewerController *ctrl_;
    Settings *settings;
    CustomizeModel *settings_window;
    QString file_name;
    int time, side_bar_width;
    QTimer *timer;

 private slots:
    void on_toolButton_toggled(bool checked);
    void on_splitter_splitterMoved(int pos, int index);
    void on_Open_model_triggered();
    void on_actionbmp_triggered();
    void on_actionjpeg_triggered();
    void on_actionSave_Screencast_triggered();
    void on_Choose_background_triggered();
    void on_Customize_Model_triggered();
    void on_Revert_model_triggered();
    void on_Update_model_triggered();
    void on_UpdateButton_released();
    void on_ResetButton_released();
    void on_actionParallel_triggered();
    void on_actionCentral_triggered();

    void TimerSlot();
    void on_actionGrid_Color_triggered();
};
}  // namespace s21
#endif  // VIEWERWINDOW_H
