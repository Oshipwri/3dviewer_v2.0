#ifndef GEOMETRYENGINE_H
#define GEOMETRYENGINE_H

#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

#include "../controller/viewerctrl.h"
#include "settings.h"

namespace s21 {

class GeometryEngine : protected QOpenGLFunctions {
 public:
    GeometryEngine(QString, ViewerController *);
    virtual ~GeometryEngine();
    void drawGeometry(QOpenGLShaderProgram *, QVector4D,
                      Settings::TypeOfVertices);
     size_t get_num_vertices() { return num_vert; }
     size_t get_num_edges() { return num_edgs; }

 private:
    void initObj();
    QString path;
    ViewerController *ctrl_;
    QOpenGLBuffer IndexBuf, VertexBuf;
    size_t num_vert, num_edgs;
    float max;

};
}  // namespace s21
#endif  // GEOMETRYENGINE_H
