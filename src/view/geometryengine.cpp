#include "geometryengine.h"

namespace s21 {

GeometryEngine::GeometryEngine(QString path_, ViewerController *controller)
    : path(path_),
      ctrl_(controller),
      IndexBuf(QOpenGLBuffer::IndexBuffer),
      max(1.0) {
    initializeOpenGLFunctions();
    VertexBuf.create();
    IndexBuf.create();

    initObj();
}

GeometryEngine::~GeometryEngine() { IndexBuf.destroy(), VertexBuf.destroy(); }

void GeometryEngine::initObj() {
    QList<QVector3D> coords;
    QList<GLuint> vertices;

    ctrl_->get_data(path, &coords, &vertices, &max);

    VertexBuf.bind();
    VertexBuf.allocate(coords.constData(), coords.size() * sizeof(QVector3D));

    IndexBuf.bind();
    IndexBuf.allocate(vertices.constData(), vertices.size() * sizeof(GLuint));

    num_vert = ctrl_->get_num_vertices();
    num_edgs = ctrl_->get_num_edges();
}

void GeometryEngine::drawGeometry(QOpenGLShaderProgram *program,
                                  QVector4D pointColor,
                                  Settings::TypeOfVertices p_type) {
    int vertexLocation = program->attributeLocation("a_position");

    VertexBuf.bind();
    program->setUniformValue("adjust", max / 8);

    program->enableAttributeArray(vertexLocation);

    program->setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3,
                                sizeof(QVector3D));
    IndexBuf.bind();

    glDrawElements(GL_TRIANGLES, num_edgs, GL_UNSIGNED_INT, NULL);

    IndexBuf.release();

    // apply the rest of point settings
    if (p_type != Settings::TypeOfVertices::None) {
        if (p_type == Settings::TypeOfVertices::Circle) {
#ifdef __APPLE__
            glEnable(GL_POINT_SMOOTH);
#else
            program->setUniformValue("linux_circle", true);
#endif
        }

        program->setUniformValue("fragment_color", pointColor);

        glDrawArrays(GL_POINTS, 0, num_vert);

        if (p_type == Settings::TypeOfVertices::Circle) {
#ifdef __APPLE__
            glDisable(GL_POINT_SMOOTH);
#else
            program->setUniformValue("linux_circle", false);
#endif
        }
    }
    program->disableAttributeArray(vertexLocation);
    VertexBuf.release();
}
}  // namespace s21
