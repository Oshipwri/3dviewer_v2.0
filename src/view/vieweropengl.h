#ifndef VIEWEROPENGL_H
#define VIEWEROPENGL_H

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif

#include <QBasicTimer>
#include <QMatrix4x4>
#include <QMouseEvent>
#include <QOpenGLWidget>
#include <QQuaternion>

#include "../controller/viewerctrl.h"
#include "geometryengine.h"
#include "settings.h"

namespace Ui {
class GlWidget;
}

namespace s21 {

struct modelTransform {
    QVector3D translate;
    QVector3D scale;
    QVector3D rotate;
};

class ViewerOpenGL : public QOpenGLWidget, protected QOpenGLFunctions {
    Q_OBJECT

 public:
    ViewerOpenGL(QWidget *parent);
    ~ViewerOpenGL();
    bool set_path(const QString);
    void set_default_view();
    void applyTransformations(modelTransform *);
    size_t get_num_verices();
    size_t get_num_edges();
    s21::Settings *settings;
    void force_resize() {
        resizeGL(width(), height());
        update();
    }

    void set_controller(ViewerController *controller);

 protected:
    void mousePressEvent(QMouseEvent *e) override;
    void mouseReleaseEvent(QMouseEvent *e) override;
    void timerEvent(QTimerEvent *e) override;
    void wheelEvent(QWheelEvent *wheel) override;

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void initShaders();
    void initGrid();

 private:
    qreal aspect, angularSpeed;
    QBasicTimer timer;
    GeometryEngine *geometries;
    QOpenGLShaderProgram model_program, grid_program;
    QOpenGLBuffer gridDataBuf;
    QMatrix4x4 projection, gridMatrix;
    QVector2D mousePressPosition;
    QQuaternion rotation;

    /* Model settings, slices - number of lines in a grid*/
    QVector3D rotateAxis, scaleAxis, translateAxis;
    QVector4D lineColor, pointColor;
    unsigned lineWidth, slices, pointSize;
    bool perspective, lineStrippled;
    ViewerController *controller_;
    QVector4D toVec4(QColor clr) { return QVector4D(clr.redF(), clr.greenF(), clr.blueF(), clr.alphaF()); }
};

}  // namespace s21

#endif  // VIEWEROPENGL_H
