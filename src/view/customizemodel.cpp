#include "customizemodel.h"

#include <QColorDialog>

#include "ui_customizemodel.h"

namespace s21 {
CustomizeModel::CustomizeModel(Settings *settings, QWidget *parent)
    : QDialog(parent), ui(new Ui::CustomizeModel), settings_(settings) {
    ui->setupUi(this);
    load_settings();
}

CustomizeModel::~CustomizeModel() { delete ui; }

void CustomizeModel::on_edgeThickness_valueChanged(int value) {
    ui->sliderValue->setText(QString::number(value));
    settings_->set_edge_thickness(value);
}

void CustomizeModel::on_EdgeColor_released() {
    QColor color = QColorDialog::getColor(settings_->get_edge_color(), this);
    if (color.isValid()) {
        settings_->set_edge_color(color);
    }
}

void CustomizeModel::on_vertexSize_valueChanged(int value) {
    ui->SizeVerticesVal->setText(QString::number(value));
    settings_->set_vertex_size(value);
}

void CustomizeModel::on_VerticesColor_released() {
    QColor color = QColorDialog::getColor(settings_->get_vertex_color(), this);
    if (color.isValid()) {
        settings_->set_vertex_color(color);
    }
}

void CustomizeModel::on_solid_clicked() {
    settings_->set_edges_type(Settings::TypeOfEdges::Solid);
}

void CustomizeModel::on_dashed_clicked() {
    settings_->set_edges_type(Settings::TypeOfEdges::Dashed);
}

void CustomizeModel::on_VerticesNoneButton_clicked() {
    settings_->set_vertices_type(Settings::TypeOfVertices::None);
}

void CustomizeModel::on_VerticesCircleButton_clicked() {
    settings_->set_vertices_type(Settings::TypeOfVertices::Circle);
}

void CustomizeModel::on_VerticesSquareButton_clicked() {
    settings_->set_vertices_type(Settings::TypeOfVertices::Square);
}

void CustomizeModel::load_settings() {
    if (settings_->get_edges_type() == Settings::TypeOfEdges::Solid) {
        ui->solid->setChecked(true);
    } else {
        ui->dashed->setChecked(true);
    }

    ui->edgeThickness->setValue(settings_->get_edge_thickness());

    if (settings_->get_vertices_type() == Settings::TypeOfVertices::None) {
        ui->VerticesNoneButton->setChecked(true);
    } else if (settings_->get_vertices_type() == Settings::TypeOfVertices::Circle) {
        ui->VerticesCircleButton->setChecked(true);
    } else {
        ui->VerticesSquareButton->setChecked(true);
    }

    ui->vertexSize->setValue(settings_->get_vertex_size());
}

}  // namespace s21
