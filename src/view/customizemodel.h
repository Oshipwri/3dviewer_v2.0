#ifndef CUSTOMIZEMODEL_H
#define CUSTOMIZEMODEL_H

#include <QDialog>

#include "../controller/viewerctrl.h"
#include "settings.h"

namespace Ui {
class CustomizeModel;
}
namespace s21 {
class CustomizeModel : public QDialog {
    Q_OBJECT

 public:
    explicit CustomizeModel(Settings *settings, QWidget *parent = nullptr);
    ~CustomizeModel();

 private slots:
    void on_edgeThickness_valueChanged(int value);

    void on_EdgeColor_released();

    void on_vertexSize_valueChanged(int value);

    void on_VerticesColor_released();

    void on_solid_clicked();

    void on_dashed_clicked();

    void on_VerticesCircleButton_clicked();

    void on_VerticesSquareButton_clicked();

    void on_VerticesNoneButton_clicked();

 private:
    Ui::CustomizeModel *ui;
    //    ViewerController *ctrl_;
    Settings *settings_;
    void load_settings();
};
}  // namespace s21

#endif  // CUSTOMIZEMODEL_H
