#ifndef SETTINGS_H
#define SETTINGS_H

#include <QOpenGLWidget>
#include <QSettings>
#include <QWidget>

namespace s21 {
class Settings {
 public:
    Settings(QOpenGLWidget *openGLWidget);
    void save_settings();
    void load_settings();

    enum Projection { Parallel, Central };
    enum TypeOfEdges { Solid, Dashed };
    enum TypeOfVertices { None, Circle, Square };
    //    enum
    QColor get_edge_color();
    QColor get_vertex_color();
    QColor get_background_color();
    QColor get_grid_color();

    Projection get_projection();
    TypeOfEdges get_edges_type();
    TypeOfVertices get_vertices_type();

    int get_edge_thickness();
    int get_vertex_size();

    void set_projection(Projection projection);
    void set_edges_type(TypeOfEdges edges_type);
    void set_edge_thickness(int edge_thickness);
    void set_edge_color(QColor edge_color);

    void set_vertices_type(TypeOfVertices vertices_type);
    void set_vertex_size(int vertex_size);
    void set_vertex_color(QColor vertex_color);

    void set_background_color(QColor background_color);
    void set_grid_color(QColor grid_color);

    QSettings current_settings;

 private:
    QOpenGLWidget *openGLWidget_;

    Projection projection_;

    TypeOfEdges edges_type_;
    int edge_thickness_;
    QColor edge_color_;

    TypeOfVertices vertices_type_;
    int vertex_size_;
    QColor vertex_color_;
    QColor background_color_;
    QColor grid_color_;
};

}  // namespace s21

#endif  // SETTINGS_H
