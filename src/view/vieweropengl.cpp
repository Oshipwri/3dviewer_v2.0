#include "vieweropengl.h"

namespace s21 {
ViewerOpenGL::ViewerOpenGL(QWidget *parent)
    : QOpenGLWidget(parent), geometries(nullptr) {
    set_default_view();
    settings = new Settings(this);
}

ViewerOpenGL::~ViewerOpenGL() {
    makeCurrent();
    delete geometries;
    gridDataBuf.destroy();
    doneCurrent();
}

void ViewerOpenGL::initializeGL() {
    initializeOpenGLFunctions();

    initShaders();

    glEnable(GL_DEPTH_TEST);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glLineStipple(10, 0x1111);

    timer.start(12, this);

    slices = 200;

    initGrid();

    gridMatrix.translate(0.0, 0.0, -5.0);
    gridMatrix.rotate(180, 0, 1.0, -0.5);
    gridMatrix.rotate(45, 0, 0, 1);
    gridMatrix.scale(slices / 10);
}

void ViewerOpenGL::resizeGL(int w, int h) {
    aspect = qreal(w) / qreal(h ? h : 1);
    const qreal zNear = 0.1, zFar = slices / 10, fov = 45.0;

    projection.setToIdentity();

    if (settings->get_projection() == Settings::Projection::Central)
        perspective = true;
    if (settings->get_projection() == Settings::Projection::Parallel)
        perspective = false;

    if (perspective) {
        projection.perspective(fov, aspect, zNear, zFar);
    } else {
        qreal top{}, bottom{}, right{}, left{};
        if (w > h) {
            top = 2.0f;
            bottom = -top;
            right = top * aspect;
            left = -right;
        } else {
            right = 2.0f;
            left = -right;
            top = right / aspect;
            bottom = -top;
        }
        projection.ortho(left, right, bottom, top, zNear, zFar);
    }
}

void ViewerOpenGL::paintGL() {
    QColor backColor(settings->get_background_color());
    glClearColor(backColor.redF(), backColor.greenF(), backColor.blueF(), backColor.alphaF());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    grid_program.bind();

    QVector4D gridColor = toVec4(settings->get_grid_color());

    grid_program.setUniformValue("mvp_matrix", projection * gridMatrix);
    grid_program.setUniformValue("grid_color", gridColor);

    gridDataBuf.bind();

    int vertexLocation = grid_program.attributeLocation("qt_Vertex");
    grid_program.enableAttributeArray(vertexLocation);
    grid_program.setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 2,
                                    sizeof(QVector2D));

    GLuint buf_size = gridDataBuf.size() / sizeof(QVector2D);
    glLineWidth(1);
    glDrawArrays(GL_LINES, 0, buf_size - 4);

    glLineWidth(4);
    glDrawArrays(GL_LINES, buf_size - 4, buf_size);

    if (geometries) {
        QMatrix4x4 dynamic_matrix;

        dynamic_matrix.translate(0, 0, -5.0);
        dynamic_matrix.rotate(180, 0, 1.0, -0.5);
        dynamic_matrix.rotate(45, 0, 0, 1);

        dynamic_matrix.translate(translateAxis);
        dynamic_matrix.scale(scaleAxis);
        dynamic_matrix.rotate(rotation);

        model_program.bind();
        model_program.setUniformValue("mvp_matrix",
                                      projection * dynamic_matrix);

        // apply line settings and point size + auto cull
        lineColor = toVec4(settings->get_edge_color());
        model_program.setUniformValue("fragment_color", lineColor);
        int vertex_size = settings->get_vertex_size();
        int edge_thickness = settings->get_edge_thickness();
        glLineWidth(edge_thickness * 0.5 + 1);

        Settings::TypeOfEdges line_type = settings->get_edges_type();
        if (line_type == Settings::TypeOfEdges::Dashed) {
            glEnable(GL_LINE_STIPPLE);
        }

        glPointSize(vertex_size);

        if (settings->get_vertices_type() == Settings::TypeOfVertices::None)
            glEnable(GL_CULL_FACE);

        pointColor = toVec4(settings->get_vertex_color());
        geometries->drawGeometry(&model_program, pointColor,
                                 settings->get_vertices_type());
        model_program.release();

        if (settings->get_vertices_type() == Settings::TypeOfVertices::None)
            glDisable(GL_CULL_FACE);

        if (line_type == Settings::TypeOfEdges::Dashed) {
            glDisable(GL_LINE_STIPPLE);
        }
    }
}

void ViewerOpenGL::set_default_view() {
    angularSpeed = 0.0;
    scaleAxis = {0.1, 0.1, 0.1};
    translateAxis = {0.0, 0.0, 0.0};
    rotateAxis = {1, 0, 0};
    rotation = {1, 0, 0, 0};
    lineWidth = 2;
    lineColor = {0.121, 0.239, 0.639, 1.0};
    lineStrippled = false;
    pointSize = 10.0;
    pointColor = {1.0, 0.0, 0.0, 1.0};
    perspective = true;
}

void ViewerOpenGL::initShaders() {
    // Compile vertex shader
    if (!model_program.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                               ":/Shaders/model_vertex.glsl"))
        close();

    // Compile fragment shader
    if (!model_program.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                               ":/Shaders/model_fragment.glsl"))
        close();

    // Link shader pipeline
    if (!model_program.link())
        close();

    // Compile vertex shader
    if (!grid_program.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                              ":/Shaders/grid_vertex.glsl"))
        close();

    // Compile fragment shader
    if (!grid_program.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                              ":/Shaders/grid_fragment.glsl"))
        close();

    // Link shader pipeline
    if (!grid_program.link()) close();
}

void ViewerOpenGL::wheelEvent(QWheelEvent *wheel) {
    if (wheel->angleDelta().y() > 0) {
        for (int i = 0; i < 3; ++i) scaleAxis[i] *= 1.1;
    } else {
        for (int i = 0; i < 3; ++i) scaleAxis[i] *= 0.9;
    }
    this->update();
}

void ViewerOpenGL::initGrid() {
    QList<QVector2D> vertices;
    vertices.reserve(slices * 4);

    for (unsigned i = 0; i <= slices / 2 - 1; ++i) {
        GLfloat x = (float)i / (float)slices;
        vertices.push_back(QVector2D(x * 2.0f - 1.0f, -1.0f));
        vertices.push_back(QVector2D(x * 2.0f - 1.0f, 1.0f));

        GLfloat y = (float)i / (float)slices;
        vertices.push_back(QVector2D(-1.0f, y * 2.0f - 1.0f));
        vertices.push_back(QVector2D(1.0f, y * 2.0f - 1.0f));
    }

    for (unsigned i = slices / 2 + 1; i <= slices; ++i) {
        GLfloat x = (float)i / (float)slices;
        vertices.push_back(QVector2D(x * 2.0f - 1.0f, -1.0f));
        vertices.push_back(QVector2D(x * 2.0f - 1.0f, 1.0f));

        GLfloat y = (float)i / (float)slices;
        vertices.push_back(QVector2D(-1.0f, y * 2.0f - 1.0f));
        vertices.push_back(QVector2D(1.0f, y * 2.0f - 1.0f));
    }

    // add central lines
    float i = slices / 2;
    GLfloat x = (float)i / (float)slices;
    vertices.push_back(QVector2D(x * 2.0f - 1.0f, -1.0f));
    vertices.push_back(QVector2D(x * 2.0f - 1.0f, 1.0f));

    GLfloat y = (float)i / (float)slices;
    vertices.push_back(QVector2D(-1.0f, y * 2.0f - 1.0f));
    vertices.push_back(QVector2D(1.0f, y * 2.0f - 1.0f));

    gridDataBuf.create();
    gridDataBuf.bind();
    gridDataBuf.allocate(vertices.constData(),
                         vertices.size() * sizeof(QVector2D));
}

bool ViewerOpenGL::set_path(const QString path_) {
    QString path = path_.toStdString().c_str();
    if (geometries) delete geometries;
    geometries = new GeometryEngine(path, controller_);
    update();
    return (geometries != nullptr);
}

void ViewerOpenGL::mousePressEvent(QMouseEvent *e) {
    mousePressPosition = QVector2D(e->position());
}

void ViewerOpenGL::mouseReleaseEvent(QMouseEvent *e) {
    // Mouse release position - mouse press position
    QVector2D diff = QVector2D(e->position()) - mousePressPosition;

    // Rotation axis is perpendicular to the mouse position difference
    // vector
    QVector3D n = QVector3D(-diff.y(), diff.y(), -diff.x()).normalized();

    // Accelerate angular speed relative to the length of the mouse sweep
    qreal acc = diff.length() / 200.0;

    // Calculate new rotation axis as weighted sum
    rotateAxis = (rotateAxis * angularSpeed + n * acc).normalized();

    // Increase angular speed
    angularSpeed += acc;
}

void ViewerOpenGL::timerEvent(QTimerEvent *) {
    // Decrease angular speed (friction)
    angularSpeed *= 0.97;

    // Stop rotation when speed goes below threshold
    if (angularSpeed < 0.01) {
        angularSpeed = 0.0;
    } else {
        // Update rotation
        rotation =
            QQuaternion::fromAxisAndAngle(rotateAxis, angularSpeed) * rotation;
        // Request an update
        update();
    }
}

void ViewerOpenGL::applyTransformations(modelTransform *transformations) {
    for (int i = 0; i < 3; ++i) scaleAxis[i] *= transformations->scale[i];

    for (int i = 0; i < 3; ++i)
        translateAxis[i] += (i != 1) ? -transformations->translate[i] / 5
                                     : transformations->translate[i] / 5;

    for (int i = 0; i < 3; ++i) {
        if (i == 0) rotateAxis = {1, 0, 0};
        if (i == 1) rotateAxis = {0, 1, 0};
        if (i == 2) rotateAxis = {0, 0, 1};
        rotation = QQuaternion::fromAxisAndAngle(rotateAxis,
                                                 transformations->rotate[i]) *
                   rotation;
    }

    update();
}

size_t ViewerOpenGL::get_num_verices() {
    size_t num_verices{};
    if (geometries) num_verices = geometries->get_num_vertices();
    return num_verices;
}

size_t ViewerOpenGL::get_num_edges() {
    size_t num_edges{};
    if (geometries) num_edges = geometries->get_num_edges();
    return num_edges;
}

void ViewerOpenGL::set_controller(ViewerController *controller) {
    controller_ = controller;
}

}  // namespace s21
