#include "viewerwindow.h"

namespace s21 {
ViewerWindow::ViewerWindow(ViewerController *ctrl, QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::ViewerWindow),
      ctrl_(ctrl),
      side_bar_width(205) {
    ui->setupUi(this);
    ui->splitter->setCollapsible(0, false);
    ui->splitter->setSizes(QList<int>() << 680 << 205);
    ui->splitter->setStretchFactor(0, 1);
    ui->statusBar->addWidget(ui->gif_message);
    ui->statusBar->addWidget(ui->lcdNumber);
    ui->statusBar->addPermanentWidget(ui->toolButton);
    ui->gif_message->hide();
    ui->lcdNumber->hide();
    settings = ui->openGLWidget->settings;
    ui->openGLWidget->set_controller(ctrl_);
    settings_window = new CustomizeModel(settings);
    time = 0;
    timer = new QTimer(this);
    (settings->get_projection() == Settings::Central)
        ? ui->actionCentral->toggle()
        : ui->actionParallel->toggle();
    connect(timer, SIGNAL(timeout()), this, SLOT(TimerSlot()));
}

ViewerWindow::~ViewerWindow() {
    settings->save_settings();
    delete ui;
    delete settings_window;
    delete settings;
}

void ViewerWindow::on_toolButton_toggled(bool checked) {
    QList<int> currentSizes = ui->splitter->sizes();
    if (checked && currentSizes[1] != 0) {
        side_bar_width = currentSizes[1];
        currentSizes[1] = 0;
        ui->toolButton->setArrowType(Qt::LeftArrow);
    } else {
        currentSizes[1] = 205;
        ui->toolButton->setArrowType(Qt::RightArrow);
    }
    ui->splitter->setSizes(currentSizes);
}

void ViewerWindow::on_splitter_splitterMoved(int pos, int index) {
    Q_UNUSED(pos);
    Q_UNUSED(index);
    int new_side_width = ui->splitter->sizes()[1];
    bool hidden = ui->toolButton->isChecked();
    if ((new_side_width < side_bar_width && !hidden) ||
        (new_side_width > 0 && hidden)) {
        ui->toolButton->toggle();
        ui->toolButton->setArrowType((hidden) ? Qt::RightArrow : Qt::LeftArrow);
    }
}

void ViewerWindow::on_Open_model_triggered() {
    QString selfilter = tr("OBJ (*.obj)");
    const QString path = QFileDialog::getOpenFileName(
        this, "Choose .obj file", QDir::homePath(),
        tr("OBJ (*.obj)", selfilter.toStdString().c_str()));
    if (path.size() > 1) {
        if (ui->openGLWidget->set_path(path)) {
            QFileInfo fileinfo(path);
            ui->textBrowser->setText("File name: " + fileinfo.fileName());
            size_t num_v = ctrl_->get_num_vertices(),
                   num_e = ctrl_->get_num_edges();
            ui->textBrowser->append("Vertices: " + QString::number(num_v));
            ui->textBrowser->append("Edges: " + QString::number(num_e));
        } else {
            ui->textBrowser->setText("Invalid file");
        }
    }
}

void ViewerWindow::on_Customize_Model_triggered() {
    settings_window->setModal(true);
    settings_window->exec();
}

ViewerOpenGL *ViewerWindow::get_p_openGL() { return ui->openGLWidget; }

void ViewerWindow::on_Revert_model_triggered() {
    set_spinboxes_to_default();
    ui->openGLWidget->set_default_view();
    ui->openGLWidget->update();
}

void ViewerWindow::set_spinboxes_to_default() {
    ui->translate_x->setValue(0);
    ui->translate_y->setValue(0);
    ui->translate_z->setValue(0);

    ui->scale_x->setValue(1);
    ui->scale_y->setValue(1);
    ui->scale_z->setValue(1);

    ui->rotate_x->setValue(0);
    ui->rotate_y->setValue(0);
    ui->rotate_z->setValue(0);
}

void ViewerWindow::on_Update_model_triggered() {
    modelTransform transformations;
    transformations.translate =
        QVector3D(ui->translate_x->value(), ui->translate_y->value(),
                  ui->translate_z->value());
    transformations.scale = QVector3D(
        ui->scale_x->value(), ui->scale_y->value(), ui->scale_z->value());
    transformations.rotate = QVector3D(
        ui->rotate_x->value(), ui->rotate_y->value(), ui->rotate_z->value());
    ui->openGLWidget->applyTransformations(&transformations);
}

void ViewerWindow::on_UpdateButton_released() { on_Update_model_triggered(); }

void ViewerWindow::on_ResetButton_released() { on_Revert_model_triggered(); }

void ViewerWindow::on_actionbmp_triggered() {
    QString file_name =
        QFileDialog::getSaveFileName(0, "Save as...", "", "BMP (*.bmp)");
    if (file_name.size() > 1) {
        QImage pic = this->ui->openGLWidget->grabFramebuffer();
        if (!file_name.endsWith(".bmp")) file_name += ".bmp";
        if (pic.save(file_name, "bmp")) {
            ui->textBrowser->append("BMP is created in:" + file_name);
        } else {
            ui->textBrowser->append("BMP is not created");
        }
    }
}

void ViewerWindow::on_actionjpeg_triggered() {
    QString file_name =
        QFileDialog::getSaveFileName(0, "Save as...", "", "JPG (*.jpg)");
    if (file_name.size() > 1) {
        QImage pic = this->ui->openGLWidget->grabFramebuffer();
        if (!file_name.endsWith(".jpg")) file_name += ".jpg";
        if (pic.save(file_name, "jpg")) {
            ui->textBrowser->append("JPG is created in:" + file_name);
        } else {
            ui->textBrowser->append("JPG is not created");
        }
    }
}

void ViewerWindow::on_actionSave_Screencast_triggered() {
    file_name = QFileDialog::getSaveFileName(this, "Name", "", "GIF (*.gif)");
    if (file_name.size() > 1) {
        if (!file_name.endsWith(".gif")) file_name += ".gif";
        this->ui->actionSave_Screencast->setEnabled(false);
        timer->start(100);
    }
}

void ViewerWindow::TimerSlot() {
    switch (time) {
        case 0:
            ui->gif_message->show();
            ui->lcdNumber->show();
            ui->lcdNumber->display(5);
            break;
        case 10:
            ui->lcdNumber->display(4);
            break;
        case 20:
            ui->lcdNumber->display(3);
            break;
        case 30:
            ui->lcdNumber->display(2);
            break;
        case 40:
            ui->lcdNumber->display(1);
            break;
    }
    time++;

    if (time < 10)
        this->ui->openGLWidget->grabFramebuffer()
            .scaled(640, 480, Qt::IgnoreAspectRatio)
            .save("00" + QString::number(time) + ".png");
    else
        this->ui->openGLWidget->grabFramebuffer()
            .scaled(640, 480, Qt::IgnoreAspectRatio)
            .save("0" + QString::number(time) + ".png");

    if (time == 50) {
        timer->stop();
        ui->gif_message->hide();
        ui->lcdNumber->hide();
        time = 0;
        std::string final =
            QString("convert -adjoin *.png " + file_name).toStdString();
        int conv_rv = system(final.c_str());
        int rm_rv = system("rm *.png");
        if (conv_rv != -1 && rm_rv != -1) {
            ui->textBrowser->append("GIF is created in:" + file_name);
        } else {
            ui->textBrowser->append("GIF is not created");
        }
        this->ui->actionSave_Screencast->setEnabled(true);
    }
}

void ViewerWindow::on_Choose_background_triggered() {
    QColor color =
        QColorDialog::getColor(settings->get_background_color(), this);
    if (color.isValid()) {
        settings->set_background_color(color);
    }
}

void ViewerWindow::on_actionParallel_triggered() {
    if (ui->actionCentral->isChecked()) ui->actionCentral->toggle();
    settings->set_projection(Settings::Parallel);
    ui->openGLWidget->force_resize();
}

void ViewerWindow::on_actionCentral_triggered() {
    if (ui->actionParallel->isChecked()) ui->actionParallel->toggle();
    settings->set_projection(Settings::Central);
    ui->openGLWidget->force_resize();
}

void ViewerWindow::on_actionGrid_Color_triggered() {
    QColor color = QColorDialog::getColor(settings->get_grid_color(), this);
    if (color.isValid()) {
        settings->set_grid_color(color);
    }
}

}  // namespace s21
