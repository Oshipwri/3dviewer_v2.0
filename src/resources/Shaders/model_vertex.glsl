#version 120

#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif
attribute vec3 a_position;

uniform mat4 mvp_matrix;
uniform float adjust;


void main()
{
    gl_Position = mvp_matrix * vec4(a_position.x / adjust, a_position.y / adjust, a_position.z / adjust, 1.0);

}
