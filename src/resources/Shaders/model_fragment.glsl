#version 120

#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform vec4 fragment_color;
uniform bool linux_circle;


void main() {

    if (linux_circle == true) {
        vec2 coord = gl_PointCoord - vec2(0.5);
           if (length(coord) > 0.5)
                discard;
    }

    gl_FragColor = fragment_color;

}
