#version 120

#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

attribute vec2 qt_Vertex;

uniform mat4 mvp_matrix;



void main() {
    gl_Position = mvp_matrix * vec4(qt_Vertex, 0.0,  1.0);
}
