#version 120

#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform vec4 grid_color;

void main(void)
{
    gl_FragColor = grid_color;
}
